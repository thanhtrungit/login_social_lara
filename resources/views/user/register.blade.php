<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css" />
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" />

<div class="container">
    <div class="row">
        <aside class="col-sm-4"></aside>
        <!-- col.// -->
        <aside class="col-sm-4">
            <br>
            <p>Login demo SNS</p>
            <div class="card">
                <article class="card-body">
                    <a href="{{ route("login") }}" class="float-right btn btn-outline-primary">Login</a>
                    <h4 class="card-title mb-4 mt-1">Register</h4>
                    <p>
                        <a href="{{ route('login_fb',['provider' => 'facebook']) }}" class="btn btn-block btn-outline-primary"> <i class="fab fa-facebook-f"></i> Login via facebook</a>
                        <a href="{{ route('login_fb',['provider' => 'google']) }}" class="btn btn-block btn-outline-danger"> <i class="fab fa-google"></i> Login via Google</a>
                    </p>
                    <hr />
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="{{ route("postregister") }}">
                        @csrf
                        <div class="form-group">
                            <input name="name" class="form-control" placeholder="Name" type="text" />
                        </div>
                        <div class="form-group">
                            <input name="email" class="form-control" placeholder="Email" type="email" />
                        </div>
                        <div class="form-group">
                            <input name="password" class="form-control" placeholder="******" type="password" />
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">Register</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </article>
            </div>
        </aside>
        <aside class="col-sm-4"></aside>
    </div>
</div>
<!--container end.//-->