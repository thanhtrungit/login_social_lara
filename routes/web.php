<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login','UserController@login')->name("login");
Route::post('/login','UserController@postlogin')->name("postlogin");
Route::get('/register','UserController@register')->name("register");
Route::post('/register','UserController@postregister')->name("postregister");
Route::get('/logout','UserController@logout')->name("logout");
Route::get('/auth/redirect/{provider}', 'SocialController@redirect')->name("login_fb");
Route::get('/callback/{provider}', 'SocialController@callback');

Route::get('/home', 'UserController@home')->middleware('auth');
Route::get('/map', 'UserController@map');
