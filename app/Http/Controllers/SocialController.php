<?php

namespace App\Http\Controllers;

use App\SocialUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;

class SocialController extends Controller
{
    public function redirect($provider){
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider){
        $getInfo = Socialite::driver($provider)->user();
        $user = $this->createUser($getInfo,$provider);
        auth()->login($user);
        return redirect()->to('/home');
    }

    function createUser($getInfo,$provider){
        $user = User::where('email', $getInfo->email)->first();
        if (!$user) {
            $user = User::create([
                'name'     => $getInfo->name,
                'email'    => $getInfo->email,
            ]);
        }
        $social_user = SocialUser::where([
            'user_id' => $user->id,
            'provider' => $provider,
            'provider_id' => $getInfo->id,
            ])->first();
        if(empty($social_user)){
            $newuser = SocialUser::create([
                'user_id' => $user->id,
                'provider' => $provider,
                'provider_id' => $getInfo->id,
            ]);
        }
        return $user;
    }
}
