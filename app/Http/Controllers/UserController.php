<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;

class UserController extends Controller
{
    public function login(){
        return view('user.login');
    }

    public function postlogin(Request $request){
        $validator = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $errors = [];
        $credentials = $request->only('email', 'password');
        if(Auth::attempt($credentials)){
            return redirect()->to('/home');
        }else{
            $errors[] = "Login incorrect";
        }
        return view('user.login',[
            'err' => $errors
        ]);
    }

    public function register(){
        return view('user.register');
    }

    public function postregister(Request $request){
        $validator = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required',
        ]);
        $user = new User();
        $user->name = $request->get("name");
        $user->email = $request->get("email");
        $user->password = bcrypt($request->get("password"));
        $user->save();
        return redirect()->to('/login');
    }

    public function logout(){
        Auth::logout();
        return redirect('/login');
    }

    public function home(){
        return view('user.home');
    }
    public function map(){
        return view('user.map');
    }
}
