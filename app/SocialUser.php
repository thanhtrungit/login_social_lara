<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialUser extends Model
{
    protected $table = 'social_user';
    protected $fillable = [
        'user_id', 'provider', 'provider_id'
    ];
}
